import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {};
const action = {};
const mutations = {};

export default new Vuex.Store({
  ...state,
  ...action,
  ...mutations
})