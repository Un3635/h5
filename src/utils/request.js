import Vue from 'vue';
import axios from 'axios';
import qs from 'qs';
// Vue.prototype.$toast.fail('请求异常')
let baseURL = "";
var request = axios.create({
  baseURL: baseURL,
  timeout: 60 * 1000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  }
});

let requestFn = (config) => {
  const time = new Date().getTime();
  // 请求默认添加 time 处理请求缓存
  config.params = config.params || {};
  config.params.time = time;
  // post delete 等请求 添加参数
  if (config.method.toLocaleLowerCase() != 'get') {
    config.data = config.data || {};


    if (config.headers['Content-Type'].indexOf('application/x-www-form-urlencoded') >= 0) {
      config.data = qs.stringify(config.data);
    }
  }

  return config;
}

let requestError = (err) => {
  Vue.prototype.$toast.fail('请求异常')
  console.warn('接口请求异常 >>>>', err)
  return Promise.reject(err);
}

// 请求拦截
request.interceptors.request.use(requestFn, requestError);

let responseFn = (res) => {
  let { data } = res;
  if (data.code != undefined) {
    if (data.code != 200) {
      Vue.prototype.$toast.fail(data.msg || '未知错误')
      console.error(`接口响应错误 >>>> ${res.request.responseURL}`, data);
      return Promise.reject(data);
    }
  }
  return data;
}

let responseError = (err) => {
  Vue.prototype.$toast.fail('响应异常')
  console.warn('接口响应异常 >>>>', err)
  return Promise.reject(err);
}
// 响应拦截
request.interceptors.response.use(responseFn, responseError);

Vue.prototype.$request = request;

export {
  request
}
