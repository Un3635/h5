import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const Demo = () => import("../pages/demo");

const router = new Router({
  mode: 'hash',
  routes: [
    {
      name: "demo",
      path: '/demo',
      component: Demo
    }
  ]
})

export default router;