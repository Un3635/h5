# h5

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### master分支
集成了以下第三方库，可直接使用
1. vant ui库
2. axios
3. vue-router
4. vuex

### rem 分支
在master 分支上 使用rem 布局

### viewport 分支
在master 分支上 使用vw 布局